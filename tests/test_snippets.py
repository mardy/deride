#
# Copyright (c) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
#
# Distributed under the MIT software license, see the accompanying
# file LICENSE or http://www.opensource.org/licenses/mit-license.php.

import pytest
from subprocess import Popen
import re
import sys


class Deride:
    def __init__(self):
        pass

    def run_single(self, input_h, output_dir, extra_args=[]):
        basename = input_h.purebasename
        args = [
            sys.executable,
            '-m', 'deride',
            *extra_args,
            '--output', str(output_dir),
            str(input_h),
        ]
        process = Popen(args, stdout=sys.stdout, stderr=sys.stdout)
        process.wait()
        return ('mock_' + basename + '.cpp',
                'mock_' + basename + '.h')


def simplify_code(text):
    """Strip common indent and remove empty lines and comments"""
    lines = text.split('\n')
    if not lines:
        return text
    # Assume that the first line is unindented
    indent = None
    simplified = []
    comment_start = re.compile(r' */\*.*')
    comment_end = re.compile(r'.*\*/')
    in_comment = False
    for line in lines:
        if indent is None and line:
            indent = len(line) - len(line.lstrip())
        l = line[indent:]
        if not in_comment:
            m = comment_start.match(l)
            if m:
                l = l[:m.start()]
                in_comment = True
        else:
            m = comment_end.match(l)
            if m:
                l = l[m.end():]
                in_comment = False
        if l and not in_comment:
            simplified.append(l)
    return '\n'.join(simplified)


class TestSnippets(object):
    @pytest.mark.parametrize(
        'input_h,extra_args,expected_output_h,expected_output_cpp', [
            ('', [], '',
             """
                #include "mock_input.h"
                #include <algorithm>
                #include <cassert>
             """),
            # Simple class, no types
            ("""
                class MyClassPrivate;
                class MyClass {
                public:
                    void doStuff();
                private:
                    MyClassPrivate *d_ptr;
                };
                """,
                [],
                """
                #include "input.h"
                #include <functional>
                #include <unordered_map>
                #include <vector>
                class MockMyClass;
                class MyClassPrivate {
                public:
                    MyClassPrivate(int ownership);
                    virtual ~MyClassPrivate();
                    void onDoStuffCalled(std::function<void()> callback) {
                        m_doStuffCb = callback;
                    }
                    static void reset();
                protected:
                    friend class MyClass;
                    int m_ownership;
                    MyClass *pClass;
                    std::function<void()> m_doStuffCb;
                };
                class MockMyClass: public MyClassPrivate {
                public:
                    enum Ownership {
                        OwnedByClient = 0,
                        OwnedByMock,
                    };
                    MockMyClass(Ownership ownership = OwnedByClient);
                    virtual ~MockMyClass();
                    static MockMyClass *latestInstance();
                    static MockMyClass *mockFor(const MyClass *mockedObject);
                private:
                    static MockMyClass *firstUnlinkedInstance();
                    static MockMyClass *ensureMockClass();
                private:
                    friend class MyClass;
                    static std::vector<MockMyClass*> m_instances;
                    static std::unordered_map<MyClass*, MockMyClass*>
                        m_instanceMap;
                };
                """, """
    #include "mock_input.h"
    #include <algorithm>
    #include <cassert>
    std::vector<MockMyClass *> MockMyClass::m_instances;
    std::unordered_map<MyClass*, MockMyClass*>
        MockMyClass::m_instanceMap;

    MyClassPrivate::MyClassPrivate(int ownership):
        m_ownership(ownership),
        pClass(nullptr)
    {
    }
    MyClassPrivate::~MyClassPrivate()
    {
    }
    void MyClassPrivate::reset()
    {
    }

    MockMyClass::MockMyClass(Ownership ownership):
        MyClassPrivate(ownership)
    {
        m_instances.push_back(this);
    }

    MockMyClass::~MockMyClass()
    {
        auto i = std::remove(m_instances.begin(), m_instances.end(), this);
        m_instances.erase(i, m_instances.end());
    }

    MockMyClass *MockMyClass::latestInstance()
    {
        const auto i =
            std::find_if(m_instances.rbegin(), m_instances.rend(),
                         [](const MockMyClass *p) {
            return p->pClass != nullptr;
        });
        return i != m_instances.rend() ? *i : nullptr;
    }

    MockMyClass *MockMyClass::mockFor(
        const MyClass *mockedObject)
    {
        const auto i =
            m_instanceMap.find(const_cast<MyClass*>(mockedObject));
        return i != m_instanceMap.end() ? i->second : nullptr;
    }

    MockMyClass *MockMyClass::firstUnlinkedInstance()
    {
        const auto i =
            std::find_if(m_instances.begin(), m_instances.end(),
                         [](const MockMyClass *p) {
            return p->pClass == nullptr;
        });
        return i != m_instances.end() ? *i : nullptr;
    }

    MockMyClass *MockMyClass::ensureMockClass() {
        MockMyClass *existing = firstUnlinkedInstance();
        return existing ? existing : new MockMyClass(OwnedByMock);
    }
    void MyClass::doStuff() {
        MockMyClass *mock = MockMyClass::mockFor(this);
        if (mock->m_doStuffCb) {
            return mock->m_doStuffCb();
        }
    }
    """),
            # Constructor with no parameters
            ("""
                class MyClassPrivate;
                class MyClass {
                public:
                    MyClass();
                    void doStuff();
                private:
                    MyClassPrivate *d_ptr;
                };
                """,
                [],
                """
    #include "input.h"
    #include <functional>
    #include <unordered_map>
    #include <vector>
    class MockMyClass;
    class MyClassPrivate {
    public:
        MyClassPrivate(int ownership);
        virtual ~MyClassPrivate();
        void onDoStuffCalled(std::function<void()> callback) {
            m_doStuffCb = callback;
        }
        static void reset();
    protected:
        friend class MyClass;
        int m_ownership;
        MyClass *pClass;
        static std::function<void()> m_constructorCb;
        std::function<void()> m_doStuffCb;
    };
    class MockMyClass: public MyClassPrivate {
    public:
        enum Ownership {
            OwnedByClient = 0,
            OwnedByMock,
        };
        MockMyClass(Ownership ownership = OwnedByClient);
        virtual ~MockMyClass();
        static MockMyClass *latestInstance();
        static MockMyClass *mockFor(const MyClass *mockedObject);
        static void onConstructorCalled(std::function<void()> callback) {
            m_constructorCb = callback;
        }
    private:
        static MockMyClass *firstUnlinkedInstance();
        static MockMyClass *ensureMockClass();
    private:
        friend class MyClass;
        static std::vector<MockMyClass*> m_instances;
        static std::unordered_map<MyClass*, MockMyClass*>
            m_instanceMap;
    };
    """, """
    #include "mock_input.h"
    #include <algorithm>
    #include <cassert>
    std::vector<MockMyClass *> MockMyClass::m_instances;
    std::unordered_map<MyClass*, MockMyClass*>
        MockMyClass::m_instanceMap;

    MyClassPrivate::MyClassPrivate(int ownership):
        m_ownership(ownership),
        pClass(nullptr)
    {
    }
    MyClassPrivate::~MyClassPrivate()
    {
    }
    void MyClassPrivate::reset()
    {
        m_constructorCb = {};
    }

    MockMyClass::MockMyClass(Ownership ownership):
        MyClassPrivate(ownership)
    {
        m_instances.push_back(this);
    }

    MockMyClass::~MockMyClass()
    {
        auto i = std::remove(m_instances.begin(), m_instances.end(), this);
        m_instances.erase(i, m_instances.end());
    }

    MockMyClass *MockMyClass::latestInstance()
    {
        const auto i =
            std::find_if(m_instances.rbegin(), m_instances.rend(),
                         [](const MockMyClass *p) {
            return p->pClass != nullptr;
        });
        return i != m_instances.rend() ? *i : nullptr;
    }

    MockMyClass *MockMyClass::mockFor(
        const MyClass *mockedObject)
    {
        const auto i =
            m_instanceMap.find(const_cast<MyClass*>(mockedObject));
        return i != m_instanceMap.end() ? i->second : nullptr;
    }

    MockMyClass *MockMyClass::firstUnlinkedInstance()
    {
        const auto i =
            std::find_if(m_instances.begin(), m_instances.end(),
                         [](const MockMyClass *p) {
            return p->pClass == nullptr;
        });
        return i != m_instances.end() ? *i : nullptr;
    }

    MockMyClass *MockMyClass::ensureMockClass() {
        MockMyClass *existing = firstUnlinkedInstance();
        return existing ? existing : new MockMyClass(OwnedByMock);
    }

    std::function<void()> MyClassPrivate::m_constructorCb;
    MyClass::MyClass()
    {
        MockMyClass *mock = MockMyClass::ensureMockClass();
        mock->pClass = this;
        MockMyClass::m_instanceMap[this] = mock;
        if (MyClassPrivate::m_constructorCb) {
            MyClassPrivate::m_constructorCb();
        }
    }
    void MyClass::doStuff() {
        MockMyClass *mock = MockMyClass::mockFor(this);
        if (mock->m_doStuffCb) {
            return mock->m_doStuffCb();
        }
    }
    """),
        ])
    def test_simple(self, input_h, extra_args,
                    expected_output_h, expected_output_cpp, tmpdir):
        deride = Deride()
        input_file = tmpdir / "input.h"
        input_file.write_text(input_h, encoding='utf-8')
        output_dir = tmpdir / "output"
        output_dir.mkdir()
        deride_args = ['--basepath', str(tmpdir)] + extra_args
        (output_file_cpp, output_file_h) = deride.run_single(input_file,
                                                             output_dir,
                                                             deride_args)
        output_h = simplify_code((output_dir / output_file_h).
                                 read_text('utf-8'))
        output_cpp = simplify_code((output_dir / output_file_cpp).
                                   read_text('utf-8'))
        assert output_h == simplify_code(expected_output_h)
        assert output_cpp == simplify_code(expected_output_cpp)

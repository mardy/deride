#include "mock_horse.h"
#include "stable.h"

#include <algorithm>
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
    using Mock = Animals::MockHorse;

    std::list<std::string> horseNames = {
        "Tom",
        "Dick",
        "Harry",
    };

    /* We could use a vector, but let's be explicit */
    Mock *mockTom;
    Mock *mockDick;
    Mock *mockHarry;

    int createdHorses = 0;
    Animals::MockHorse::onConstructorCalled([&](const std::string &name) {
        std::cout << "Horse instantiated: " << name << std::endl;
        createdHorses++;
        if (name == "Tom") {
            mockTom = Mock::latestInstance();
        } else if (name == "Dick") {
            mockDick = Mock::latestInstance();
        } else if (name == "Harry") {
            mockHarry = Mock::latestInstance();
        } else {
            assert(false); // should not be reached
        }
    });

    Stable stable;
    stable.createHorses(horseNames);
    assert(createdHorses == 3);
    assert(stable.count() == 3);
    assert(mockTom != nullptr);
    assert(mockDick != nullptr);
    assert(mockHarry != nullptr);

    /* Prepare for mocking the jump */
    mockTom->setJumpHeightResult(1.5);
    mockDick->setJumpHeightResult(1.7);
    mockHarry->setJumpHeightResult(1.3);

    std::string highestJumper = stable.findHighestJumper();
    assert(highestJumper == "Dick");
}

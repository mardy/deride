#pragma once
#include <string>

namespace Animals {

class HorsePrivate;
class Horse {
public:
    Horse(const std::string &name);
    Horse(Horse &&other);
    virtual ~Horse();

    std::string name() const;

    void jump();
    float jumpHeight() const;

    void setColor(const std::string &colorName);
    std::string color() const;

private:
    HorsePrivate *d_ptr;
};

} // namespace

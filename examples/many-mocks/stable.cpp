#include "stable.h"

using namespace Animals;

void Stable::createHorses(const std::list<std::string> &names)
{
    for (const auto &name: names) {
        m_horses.emplace(name, Horse(name));
    }
}

int Stable::count() const
{
    return m_horses.size();
}

std::string Stable::findHighestJumper()
{
    /* Use two different loops, just to make things more interesting */
    for (auto &p: m_horses) {
        p.second.jump();
    }

    float highest = 0.0;
    std::string highestJumper;
    for (const auto &p: m_horses) {
        float height = p.second.jumpHeight();
        if (height > highest) {
            highest = height;
            highestJumper = p.first;
        }
    }

    return highestJumper;
}

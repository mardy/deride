#pragma once
#include "horse.h"

#include <list>
#include <map>
#include <string>

class Stable {
public:
    Stable() = default;

    void createHorses(const std::list<std::string> &names);

    int count() const;

    std::string findHighestJumper();

private:
    std::map<std::string,Animals::Horse> m_horses;
};

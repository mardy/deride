#pragma once
#include <memory>
#include <string>

class DatabasePrivate;
class Database {
public:
    Database(const std::string &path);
    virtual ~Database();

    bool open();

    bool setValue(const std::string &key, const std::string &value);
    std::string getValue(const std::string &key) const;

private:
    std::unique_ptr<DatabasePrivate> pimpl;
};

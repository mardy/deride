#include "database.h"

#include <lmdb.h>
#include <sys/stat.h>

class DatabasePrivate {
public:
    DatabasePrivate(const std::string &path);
    ~DatabasePrivate();

    bool open();
    bool setValue(const std::string &key, const std::string &value);
    std::string getValue(const std::string &key) const;

private:
    friend class Database;
    std::string m_dbPath;
    MDB_env *m_env;
    MDB_txn *m_readTx;
    MDB_dbi m_db;

};

DatabasePrivate::DatabasePrivate(const std::string &path):
    m_dbPath(path),
    m_env(nullptr),
    m_readTx(nullptr),
    m_db(0)
{
}

DatabasePrivate::~DatabasePrivate()
{
    if (m_db != 0) {
        mdb_dbi_close(m_env, m_db);
    }

    if (m_readTx) {
        mdb_txn_abort(m_readTx);
    }

    if (m_env) {
        mdb_env_close(m_env);
    }
}

bool DatabasePrivate::open()
{
    if (mdb_env_create(&m_env) != MDB_SUCCESS) return false;

    mdb_mode_t perms = S_IRUSR | S_IWUSR | S_IRGRP;
    int flags = MDB_WRITEMAP;

    int rc = mdb_env_open(m_env, m_dbPath.c_str(), flags, perms);
    if (rc != MDB_SUCCESS) return false;

    rc = mdb_txn_begin(m_env, nullptr, MDB_RDONLY, &m_readTx);
    if (rc != MDB_SUCCESS) return false;

    rc = mdb_dbi_open(m_readTx, nullptr, 0, &m_db);
    return rc == MDB_SUCCESS;
}

bool DatabasePrivate::setValue(const std::string &key, const std::string &value)
{
    MDB_txn *tx;
    int rc = mdb_txn_begin(m_env, nullptr, 0, &tx);
    if (rc != MDB_SUCCESS) return false;

    MDB_val mdbKey;
    MDB_val mdbVal;

    mdbKey.mv_data = const_cast<void*>(static_cast<const void*>(key.c_str()));
    mdbKey.mv_size = key.size();

    mdbVal.mv_data = const_cast<void*>(static_cast<const void*>(value.c_str()));
    mdbVal.mv_size = value.size();

    rc = mdb_put(tx, m_db, &mdbKey, &mdbVal, 0);
    if (rc != MDB_SUCCESS) return false;

    rc = mdb_txn_commit(tx);
    if (rc != MDB_SUCCESS) return false;

    // Update the read transaction
    mdb_txn_reset(m_readTx);
    mdb_txn_renew(m_readTx);
    return true;
}

std::string DatabasePrivate::getValue(const std::string &key) const
{
    MDB_val mdbKey;
    MDB_val mdbVal;

    mdbKey.mv_data = const_cast<void*>(static_cast<const void*>(key.c_str()));
    mdbKey.mv_size = key.size();

    int rc = mdb_get(m_readTx, m_db, &mdbKey, &mdbVal);
    if (rc != MDB_SUCCESS) return std::string();

    return std::string(static_cast<char*>(mdbVal.mv_data), mdbVal.mv_size);
}

Database::Database(const std::string &path):
    pimpl(new DatabasePrivate(path))
{
}

Database::~Database()
{
}

bool Database::open()
{
    return pimpl->open();
}

bool Database::setValue(const std::string &key, const std::string &value)
{
    return pimpl->setValue(key, value);
}

std::string Database::getValue(const std::string &key) const
{
    return pimpl->getValue(key);
}

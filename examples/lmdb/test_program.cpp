#include "mock_lmdb.h"
#include "database.h"

#include <algorithm>
#include <cassert>
#include <iostream>

void test_open_fail()
{
    Mock mock;

    int call_count = 0;
    mock.onMdbEnvCreateCalled([&call_count](MDB_env **env) {
        assert(env != nullptr);
        call_count++;
        return MDB_PANIC;
    });

    Database db("somewhere");
    assert(db.open() == false);
    assert(call_count == 1);
}

void test_open_ok()
{
    Mock mock;

    MDB_env *envPtr = (MDB_env*)0xdeadbeef;
    mock.onMdbEnvCreateCalled([&envPtr](MDB_env **env) {
        assert(env != nullptr);
        *env = envPtr;
        return MDB_SUCCESS;
    });

    std::string openedPath;
    unsigned int openedFlags = 0;
    mdb_mode_t openedPerms = 0;
    mock.onMdbEnvOpenCalled([&](MDB_env *env, const char *path,
                                unsigned int flags, mdb_mode_t perms) {
        openedPath = std::string(path);
        openedFlags = flags;
        openedPerms = perms;
        return MDB_SUCCESS;
    });

    /* We are lazy here, but we could check that also these two
     * functions get called with the right parameters. */
    mock.onMdbTxnBeginCalled([](MDB_env *, MDB_txn *,
                                unsigned int, MDB_txn **) {
        return MDB_SUCCESS;
    });
    mock.onMdbDbiOpenCalled([](MDB_txn *, const char *,
                               unsigned int, MDB_dbi *) {
        return MDB_SUCCESS;
    });

    MDB_env *destroyedEnv = nullptr;
    mock.onMdbEnvCloseCalled([&](MDB_env *env) {
        destroyedEnv = env;
        return MDB_SUCCESS;
    });

    {
        Database db("somewhere");
        assert(db.open() == true);
    }

    assert(destroyedEnv == envPtr);
}

int main(int argc, char **argv)
{
    test_open_fail();
    test_open_ok();

    // We can add tests for setValue() and getValue()
    return EXIT_SUCCESS;
}

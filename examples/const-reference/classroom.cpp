#include "classroom.h"

void Classroom::addTeacher(const Teacher &teacher)
{
    m_teacher = teacher;
}

std::string Classroom::teacherName() const
{
    return m_teacher.name();
}

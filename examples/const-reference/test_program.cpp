#include "classroom.h"
#include "mock_teacher.h"

#include <algorithm>
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
    MockTeacher mock;
    mock.setNameResult("Dick");

    Classroom classroom;
    Teacher teacher("Tom");
    classroom.addTeacher(teacher);

    assert(classroom.teacherName() == "Dick");
}

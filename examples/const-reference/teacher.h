#pragma once
#include <string>

class Teacher {
public:
    Teacher(const std::string &name = {});
    virtual ~Teacher();

    const std::string &name() const;

private:
    std::string m_name;
};

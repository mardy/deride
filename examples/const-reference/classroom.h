#pragma once
#include "teacher.h"

#include <string>

class Classroom {
public:
    void addTeacher(const Teacher &teacher);

    std::string teacherName() const;

private:
    Teacher m_teacher;
};

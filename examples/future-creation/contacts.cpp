#include "contacts.h"

#include "database.h"

#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

class ContactsPrivate {
public:
    ContactsPrivate(const std::string &dbName);

private:
    friend class Contacts;
    Database m_database;
    std::vector<Contacts::Contact> m_contacts;
};

ContactsPrivate::ContactsPrivate(const std::string &dbName):
    m_database("/some/path/" + dbName)
{
    while (true) {
        const std::tuple<std::string,std::string> row =
            m_database.readNextRow();
        if (std::get<0>(row).empty()) break;

        std::tm tm;
        std::istringstream ss(std::get<1>(row));
        ss >> std::get_time(&tm, "%Y-%m-%d");
        auto time = std::chrono::system_clock::from_time_t(timegm(&tm));
        m_contacts.emplace_back(Contacts::Contact { std::get<0>(row), time });
    }
}

Contacts::Contacts(const std::string &dbName):
    d_ptr(new ContactsPrivate(dbName))
{
}

Contacts::~Contacts()
{
}

int Contacts::count() const
{
    return d_ptr->m_contacts.size();
}

std::list<Contacts::Contact>
Contacts::contactsBeginningWith(const std::string &prefix) const
{
    std::list<Contact> ret;
    for (const auto contact: d_ptr->m_contacts) {
        if (contact.name.rfind(prefix, 0) == 0) {
            ret.push_back(contact);
        }
    }
    return ret;
}

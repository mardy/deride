#pragma once
#include <chrono>
#include <list>
#include <memory>
#include <string>

class ContactsPrivate;
class Contacts {
public:
    struct Contact {
        std::string name;
        std::chrono::system_clock::time_point birthTime;
    };

    Contacts(const std::string &dbName);
    virtual ~Contacts();

    int count() const;
    std::list<Contact> contactsBeginningWith(const std::string &prefix) const;

private:
    std::unique_ptr<ContactsPrivate> d_ptr;
};

#pragma once
#include <memory>
#include <string>
#include <tuple>

class DatabasePrivate;
class Database {
public:
    Database(const std::string &filePath);
    virtual ~Database();

    std::tuple<std::string,std::string> readNextRow();

private:
    std::unique_ptr<DatabasePrivate> pimpl;
};

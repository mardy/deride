#include "mock_database.h"
#include "contacts.h"

#include <algorithm>
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
    /* Initialize the mock object. Note how we can program it even *before* the
     * Database class has been instantiated. */
    MockDatabase mock;
    std::vector<std::tuple<std::string,std::string>> results = {
        std::make_tuple("Tom Sawyer", "1980-10-24"),
        std::make_tuple("Mr Bean", "1970-04-13"),
        std::make_tuple("Sandokan", "1983-01-02"),
        std::make_tuple("Don Juan", "1973-03-04"),
    };
    int row = 0;
    mock.onReadNextRowCalled([&results,&row]() {
        return row < results.size() ?
            results[row++] : std::make_tuple(std::string(), std::string());
    });


    /* Now create the Contacts class which will internally instantiate the
     * Database class, which will be automatically linked to our MockDatabase.
     */
    Contacts contacts("friends");

    assert(contacts.count() == 4);

    std::list<Contacts::Contact> list = contacts.contactsBeginningWith("San");
    assert(list.size() == 1);
    const Contacts::Contact &sandokan = list.front();
    assert(sandokan.name == "Sandokan");
    time_t tt = std::chrono::system_clock::to_time_t(sandokan.birthTime);
    tm utcTm = *gmtime(&tt);
    assert(utcTm.tm_year == 83);
    assert(utcTm.tm_mon == 0);
    assert(utcTm.tm_mday == 2);
    return EXIT_SUCCESS;
}

#include "mock_builder.h"
#include "testable.h"

#include <cassert>
#include <unordered_map>

int main(int argc, char **argv)
{
    using namespace Core;

    Watcher *usedWatcher;
    Builder::Map usedHints;
    MockBuilder::onBuildCalled([&](Watcher *watcher,
                                   const Builder::Map &hints) {
        usedWatcher = watcher;
        usedHints = hints;
        return true;
    });

    {
        Testable testable;
        testable.setName("Tom");
        testable.callBuilder();
        const Builder::Map expectedHints {
            { "name", "Tom" },
            { "age", -1 },
        };
        assert(usedHints == expectedHints);
        assert(usedWatcher == nullptr);
    }

    {
        Testable testable;
        testable.setAge(18);
        testable.callBuilder();
        const Builder::Map expectedHints {
            { "name", "" },
            { "age", 18 },
        };
        assert(usedHints == expectedHints);
        assert(usedWatcher == nullptr);
    }

    return EXIT_SUCCESS;
}

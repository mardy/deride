#include "testable.h"

#include "builder.h"

using namespace Core;

Testable::Testable():
    m_age(-1)
{
}

Testable::~Testable()
{
}

void Testable::setName(const std::string &name)
{
    m_name = name;
}

void Testable::setAge(int age)
{
    m_age = age;
}

bool Testable::callBuilder()
{
    Builder::Map map {
        { "name", m_name },
        { "age", m_age },
    };
    return Builder::build(nullptr, map);
}

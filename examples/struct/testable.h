#pragma once
#include <string>

namespace Core {

class Testable {
public:
    Testable();
    virtual ~Testable();

    void setName(const std::string &name);
    void setAge(int age);
    bool callBuilder();

private:
    std::string m_name;
    int m_age;
};

} // namespace

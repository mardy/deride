#pragma once
#include <string>
#include <unordered_map>
#include <variant>

namespace Core {

class Watcher; // Just to try something unusual

struct Builder
{
    using Variant = std::variant<int,std::string>;
    using Map = std::unordered_map<std::string, Variant>;
    static bool build(Watcher *watcher, const Map &hints);
};

} // namespace Core

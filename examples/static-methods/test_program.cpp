#include "mock_application.h"
#include "window.h"

#include <algorithm>
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
    using namespace Core;

    Application app("A name");
    MockApplication::onInstanceCalled([&]() {
        return &app;
    });

    {
        MockApplication::setNameResult("My app");
        Window window;
        assert(window.title() == "My app");
        window.setTitle("my title");
        assert(window.title() == "my title");
    }

    {
        MockApplication *mock = MockApplication::mockFor(&app);
        int windows = 0;
        mock->onAddWindowCalled([&](Window *w) {
            windows++;
        });
        mock->onRemoveWindowCalled([&](Window *w) {
            windows--;
        });

        {
            Window window;
            assert(windows == 1);
        }
        assert(windows == 0);
    }

    {
        MockApplication::reset();
        MockApplication::onInstanceCalled([&]() {
            return &app;
        });
        Window window;
        assert(window.title().empty());
    }

    return EXIT_SUCCESS;
}

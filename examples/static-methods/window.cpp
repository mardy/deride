#include "window.h"

#include "application.h"

using namespace Core;

Window::Window()
{
    Application *app = Application::instance();
    app->addWindow(this);
}

Window::~Window()
{
    Application *app = Application::instance();
    app->removeWindow(this);
}

void Window::setTitle(const std::string &title)
{
    m_title = title;
}

std::string Window::title() const
{
    return m_title.empty() ? Application::name() : m_title;
}

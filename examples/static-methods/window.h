#pragma once
#include <string>

namespace Core {

class Window {
public:
    Window();
    virtual ~Window();

    void setTitle(const std::string &title);
    std::string title() const;

private:
    std::string m_title;
};

} // namespace

#pragma once
#include <string>

namespace Core {

class Window;

class Application {
public:
    static Application *instance();

    static std::string name();

    void addWindow(Window *window);
    void removeWindow(Window *window);

    /* TODO: this should be protected, but we need to add an API to the mock
     * class to create it. */
public:
    Application(const std::string &name);
    virtual ~Application();

private:
    std::string m_name;
};

} // namespace

#pragma once
#include <functional>
#include <memory>
#include <string>

class SocketWatcherPrivate;
class SocketWatcher {
public:
    SocketWatcher(int socket);
    virtual ~SocketWatcher();

    void onDataReady(std::function<void()>);

private:
    std::unique_ptr<SocketWatcherPrivate> pimpl;
};

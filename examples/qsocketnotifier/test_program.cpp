#include "MockQSocketNotifier.h"
#include "socket_watcher.h"

#include <algorithm>
#include <cassert>
#include <iostream>

int main(int argc, char **argv)
{
    // Mock file descriptor
    int socket = 53;

    /* Instantiate the class under test */
    SocketWatcher watcher(socket);
    int call_count = 0;
    watcher.onDataReady([&call_count]() {
        call_count++;
    });

    assert(call_count == 0);

    /* Get our mock object */
    MockQSocketNotifier *mock = MockQSocketNotifier::latestInstance();
    assert(mock != nullptr);

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
    mock->emitActivated(socket, QSocketNotifier::Read);
#else
    mock->emitActivated(socket);
#endif
    assert(call_count == 1);

#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
    mock->emitActivated(socket, QSocketNotifier::Read);
#else
    mock->emitActivated(socket);
#endif
    assert(call_count == 2);

    return EXIT_SUCCESS;
}

#include "socket_watcher.h"

#include <QMetaObject>
#include <QSocketNotifier>

class SocketWatcherPrivate {
public:
    SocketWatcherPrivate(int socket);

private:
    friend class SocketWatcher;
    QSocketNotifier m_notifier;
    QMetaObject::Connection m_connection;
};

SocketWatcherPrivate::SocketWatcherPrivate(int socket):
    m_notifier(socket, QSocketNotifier::Read)
{
}

SocketWatcher::SocketWatcher(int socket):
    pimpl(new SocketWatcherPrivate(socket))
{
}

SocketWatcher::~SocketWatcher()
{
}

void SocketWatcher::onDataReady(std::function<void()> callback)
{
    if (pimpl->m_connection) {
        QObject::disconnect(pimpl->m_connection);
    }
    if (callback) {
        pimpl->m_connection = QObject::connect(&pimpl->m_notifier,
                                               &QSocketNotifier::activated,
                                               callback);
    } else {
        pimpl->m_connection = QMetaObject::Connection();
    }
}

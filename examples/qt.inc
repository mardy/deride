QMAKE = qmake

QT_INSTALL_HEADERS = $(shell qmake -query QT_INSTALL_HEADERS)
QT_INSTALL_LIBS = $(shell qmake -query QT_INSTALL_LIBS)
QT_VERSION = $(shell qmake -query QT_VERSION)

LFLAGS := $(LFLAGS) -L$(QT_INSTALL_LIBS) -lQt5Core

QT_ANNOTATE_MACROS = $(shell grep QT_ANNOTATE_ $(QT_INSTALL_HEADERS)/QtCore/qobjectdefs.h)
ifeq ($(QT_ANNOTATE_MACROS), )
$(error Qt 5.6 or later is required)
endif

SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))

DERIDE = PYTHONPATH=$(SELF_DIR)/.. python3 -m deride

DERIDE_MODULE = $(SELF_DIR)/../deride/

CXXFLAGS = -std=c++11 -g -fPIC
LFLAGS = -shared -pie
